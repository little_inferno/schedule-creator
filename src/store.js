import Vue from 'vue'
import Vuex from 'vuex'
import {client} from "./client";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user: {},
    lessons: [],
    teachers: [],
    groups: [],
    currentSchedules: Array,
    currentGroup: Object
  },
  mutations: {
    //change it >
    auth_request(state) {
      state.status = 'loading'
    },
    auth_success(state, token, user) {
      state.status = 'success';
      state.token = token;
      state.user = user;
    },
    auth_error(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = '';
      state.token = '';
    },
    //> change it

    teachers(state, payload) {
      state.teachers = payload
    },
    lessons(state, payload) {
      state.lessons = payload
    },
    groups(state, payload) {
      state.groups = payload
    },
    currentGroup(state, group) {
      state.currentGroup = group
    },
    currentSchedules(state, group) {
      state.currentSchedules = group
    }
  },
  actions: {
    login({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request');
        client.post('/auth/login', user)
          .then(resp => {
            const token = resp.data;
            localStorage.setItem('token', token.access);
            client.defaults.headers.common['Authorization'] = `Bearer  ${token.access}`;
            commit('auth_success', token.access, user.login);
            resolve(resp)
          })
          .catch(err => {
            commit('auth_error');
            localStorage.removeItem('token');
            reject(err)
          })
      })
    },
    logout({commit}) {
      return new Promise((resolve) => {
        commit('logout');
        localStorage.removeItem('token');
        resolve()
      })
    },

    loadTeachers({commit}) {
      return client.get('/teachers').then(response => commit('teachers', response.data))
    },
    loadLessons({commit}) {
      return client.get('/lessons').then(response => commit('lessons', response.data))
    },
    loadGroups({commit}) {
      return client.get('/groups').then(response => commit('groups', response.data))
    },
    editTeacher({dispatch}, payload) {
      return client.put(`/teachers/${payload.id}`, payload.to)
        .then(() => dispatch('loadTeachers'))
    },
    editLesson({dispatch}, payload) {
      return client.put(`/lessons/${payload.id}`, payload.to)
        .then(() => dispatch('loadLessons'))
    },
    editGroup({dispatch}, payload) {
      return client.put(`/groups/${payload.id}`, payload.to)
        .then(() => dispatch('loadGroups'))
    },
    deleteTeacher({dispatch}, payload) {
      return client.delete(`/teachers/${payload}`)
        .then(() => dispatch('loadTeachers'))
    },
    deleteLesson({dispatch}, payload) {
      return client.delete(`/lessons/${payload}`)
        .then(() => dispatch('loadLessons'))
    },
    deleteGroup({dispatch}, payload) {
      return client.delete(`/groups/${payload}`)
        .then(() => dispatch('loadGroups'))
    },
    addTeacher({dispatch}, payload) {
      return client.post('/teachers', payload)
        .then(() => dispatch('loadTeachers'))
    },
    addLesson({dispatch}, payload) {
      return client.post('/lessons', payload)
        .then(() => dispatch('loadLessons'))
    },
    addGroup({dispatch}, payload) {
      return client.post('/groups', payload)
        .then(() => dispatch('loadGroups'))
    },
    loadSchedules({commit}, group) {
      client.get(`/groups/${group}/schedules`)
        .then(res => commit('currentSchedules', res.data))
    },
    loadGroup({commit}, id) {
      return client.get(`/schedules/${id}`)
        .then(res => commit('currentGroup', res.data))
    },
    getLesson({dispatch, state}, id) {
      if (state.lessons.length === 0)
        return dispatch('loadLessons').then(() => state.lessons.find(i => i.id == id));
      else
        return state.lessons.find(i => i.id === id)
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    currentGroup: state => state.currentGroup,
  }
})