let types = new Map();
types.set('Practice', 'Практика');
types.set('Exam', 'Экзамен');
types.set('Laboratory', 'Лабораторная');
types.set('Lecture', 'Лекция');
types.set('Test', 'Зачет');

let typesColors = new Map();
typesColors.set('Practice', '#2196F3');
typesColors.set('Exam', '#B71C1C');
typesColors.set('Laboratory', '#009688');
typesColors.set('Lecture', '#4CAF50');
typesColors.set('Test', '#FF5722');

let graduate = new Map();
graduate.set('College', 'Колледж');
graduate.set('Bachelor', 'Бакалавриат');
graduate.set('Master', 'Магистратура');

let kind = new Map();
kind.set('Archive', 'Архивное');
kind.set('Actual', 'Текущее');
kind.set('Draft', 'Не опубликованное');

let days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

let typesProperty = Array.from(types, pair => ({
  value: pair[0],
  text: pair[1],
}));

function scheduleKind(type) {
  return kind.get(type)
}

function graduateLevel(type) {
  return graduate.get(type)
}

function lessonType(type) {
  return types.get(type)
}

function typeColor(type) {
  return typesColors.get(type)
}

let workdaySlots = [
  {begin: '08:30:00', end: '10:05:00'},
  {begin: '10:15:00', end: '11:50:00'},
  {begin: '12:35:00', end: '14:10:00'},
  {begin: '13:50:00', end: '15:25:00'},
  {begin: '16:05:00', end: '17:40:00'},
  {begin: '17:50:00', end: '19:25:00'},
];

let saturdaySlots = [
  {begin: '08:30:00', end: '10:05:00'},
  {begin: '10:15:00', end: '11:50:00'},
  {begin: '12:00:00', end: '13:40:00'},
  {begin: '13:50:00', end: '15:25:00'},
  {begin: '15:35:00', end: '17:10:00'},
  {begin: '17:20:00', end: '18:55:00'},
];

function timeSlot(number, isSaturday) {
  return isSaturday ? saturdaySlots[number - 1] : workdaySlots[number - 1]
}

export {
  lessonType,
  typesProperty,
  typeColor,
  days,
  timeSlot,
  graduateLevel,
  scheduleKind
}








