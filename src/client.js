import axios from "axios";

let client = axios.create({
  baseURL: process.env.VUE_APP_SCHEDULE_API,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Content-Type, access-control-allow-headers, access-control-allow-origin, authorization',
  }
});

function addObject(type, object) {
  return client.request({
    url: '/objects',
    method: 'post',
    params: {
      'objectType': type, 'name': object
    }
  })
}

function loadObjects(type) {
  return client.get('/objects', {params: {'objectType': type}})
}

function renameObject(type, from, to) {
  return client.request({
    url: '/objects',
    method: 'put',
    params: {'objectType': type, 'from': from, 'to': to}
  })
}

function deleteObject(type, object) {
  return client.request({
    url: '/objects',
    method: 'delete',
    params: {'objectType': type, 'name': object}
  })
}

export {client, addObject, loadObjects, renameObject, deleteObject}
