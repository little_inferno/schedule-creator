import Vue from 'vue'
import Router from 'vue-router'
import store from './store.js'
import Login from './components/LoginPage.vue'
import FullDashboard from "./components/Dashboard.vue";
import Groups from "./components/Groups";
import ScheduleDay from "./components/schedule/ScheduleDay";
import Schedules from "./components/Schedules";

Vue.use(Router);

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'groupMenu',
      meta: {
        requiresAuth: true
      },
      component: Groups
    },
    {
      path: '/schedules/:group',
      name: 'schedules',
      meta: {
        requiresAuth: true
      },
      component: Schedules,
    },
    {
      path: '/schedules/:group/edit/:id',
      name: 'editor',
      meta: {
        requiresAuth: true
      },
      component: FullDashboard,
      children: [
        {
          name: 'day',
          path: 'day/:day',
          component: ScheduleDay
        }
      ]
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next();
      return
    }
    next('/login')
  } else {
    next()
  }

});

export default router