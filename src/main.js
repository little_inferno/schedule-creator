import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';
import store from './store'
import router from "./router";
import {client} from "./client";
import Vuex from "vuex";

Vue.config.productionTip = false;

Vue.prototype.$http = client;
Vue.use(Vuex);

const token = localStorage.getItem('token');

if (token !== undefined) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer  ${token}`;
}


new Vue({
  vuetify,
  render: h => h(App),
  store,
  router
}).$mount('#app');
